$(document).ready(function () {
    $('#ru-button').removeClass('is-outlined')
    $('[lang="es"]').hide();

    $('#ru-button').click(function () {
        $('#ru-button').removeClass('is-outlined')
        $('#en-button').addClass('is-outlined')
        $('[lang="es"]').hide();
        $('[lang="ru"]').show();
    });

    $('#en-button').click(function () {
        $('#ru-button').addClass('is-outlined')
        $('#en-button').removeClass('is-outlined')
        $('[lang="es"]').show();
        $('[lang="ru"]').hide();
    });

    $('#open-modal').click(function () {
        console.log('test')
        $('#modal-send').addClass('is-active');
    });

    $('.modal-background').click(function () {
        $('#modal-send').removeClass('is-active');
    });
});