Django==3.1.7
psycopg2-binary==2.8.6

django-redis==4.12.1
redis==3.5.3
dj-database-url==0.5.0
gunicorn==20.0.4

ipython==7.21.0 # приятная работа в shell

Pillow==8.1.2 # для работы с изображениями

git+https://github.com/SibdevPro/django-hash-static.git@1.0.2 # хэширование статики
