from django.urls import reverse
from django.views.generic import TemplateView, FormView
from ..forms import ProjectsForm


class HomePageView(TemplateView):
    template_name = 'tour/index.html'


class ProjectsFormView(FormView):
    template_name = 'tour/form.html'
    form_class = ProjectsForm
    success_url = '/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.send_email()
        return super().form_valid(form)
