from django import forms
from django.core.mail import send_mail


class ProjectsForm(forms.Form):
    PROJECTS_CHOICES = (
        ('rusal', 'РУСАЛ'),
        ('invest', 'Инвестиционные проекты'),
        ('arctic', 'Арктика')
    )

    email = forms.EmailField(
        label='Ваш Email',
        max_length=100,
        widget=forms.EmailInput(attrs={'class': 'input is-success'})
    )
    projects = forms.MultipleChoiceField(
        label='Выберете проекты',
        widget=forms.CheckboxSelectMultiple(attrs={'class': 'is-success checkbox'}),
        choices=PROJECTS_CHOICES,
    )

    def send_email(self):
        email = self.cleaned_data['email']
        projects = self.cleaned_data['projects']
        choices_map = dict(self.PROJECTS_CHOICES)
        send_mail(
            'Тема письма',
            'Отправлены проекты ' + ', '.join(choices_map[project] for project in projects),
            'rimas.amga@gmail.com',
            [email],
            fail_silently=False,
        )
