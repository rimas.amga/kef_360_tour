from django.urls import path
from django.views.decorators.cache import cache_page

from .views import HomePageView, TourView, ProjectsFormView

urlpatterns = [
    path('', cache_page(60 * 15)(HomePageView.as_view()), name='home'),
    path('form/', cache_page(60 * 15)(ProjectsFormView.as_view()), name='form'),
    path('tour/<str:lang>/', cache_page(60 * 15)(TourView.as_view()), name='tour'),
]
