from django.views import View
from django.template.response import TemplateResponse

from ..constants import Languages


class TourView(View):
    TEMPLATES_MAP = {
        Languages.RUSSIAN: {
            'template': 'tour/tour.html',
            'context': {
                'node': '{node1}',
            },
        },
        Languages.ENGLISH: {
            'template': 'tour/tour.html',
            'context': {
                'node': '{node3}',
            },
        },
    }

    def get(self, request, lang=None):
        return TemplateResponse(request, **self.TEMPLATES_MAP.get(lang, Languages.RUSSIAN))
